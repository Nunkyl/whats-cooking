﻿#!/usr/bin/env python

import os
import re
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import cPickle
from nltk.stem import PorterStemmer

#read data
data = pd.read_json('train.json')
data.head() #show how data looks

recipe_num,y = data.shape
training_size = round(recipe_num*80./100.)
testing_size = recipe_num - training_size

ingredients = data['ingredients'] #ripping out only the ingredients

#add some preprocessing: stemming words
ps = PorterStemmer()
for line in ingredients:
    for word in line:
        ps.stem(word)
        
words_list = [' '.join(x) for x in ingredients]

#split data set
train = words_list[:int(training_size)]
test = words_list[-1*int(testing_size):]
 
#create a bag of words for both sets       
vectorizer = CountVectorizer() #not restricting number of features

bag_of_training_words = vectorizer.fit(train)
bag_of_training_words = vectorizer.transform(train).toarray()
print(bag_of_training_words.shape)

bag_of_testing_words = vectorizer.fit(train)
bag_of_testing_words = vectorizer.transform(test).toarray()
print(bag_of_testing_words.shape)

#separate cuisine data
cuisine = data["cuisine"].values.tolist()
cuisine_for_training = cuisine[:int(training_size)]
cuisine_for_testing = cuisine[-1*int(testing_size):]

# 1. Random Forest Classifier

# train model
forest = RandomForestClassifier(n_estimators = 1000)
forest = forest.fit( bag_of_training_words, cuisine_for_training )

# predict results
result1 = forest.predict(bag_of_training_words)
result2 = forest.predict(bag_of_testing_words)

# count accuracy
d = [] 
for i in range(len(result1)):
    d.append(result1[i] == cuisine_for_training[i])
    
s = float(sum(d))
dif = float(s/len(d))*100.
print dif

d = [] 
for i in range(len(result2)):
    d.append(result2[i] == cuisine_for_testing[i])
    
s = float(sum(d))
dif = float(s/len(d))*100.
print dif

# results
# (31819, 2890)
# (7955, 2890)
# 99.9685722367
# 76.4676304211

# 1000 trees
# (31819, 2890)
# (7955, 2890)
# 99.9685722367
76.5933375236

