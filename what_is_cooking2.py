﻿#!/usr/bin/env python

import os
import re
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
import cPickle
from nltk.stem import PorterStemmer
from sklearn import svm
from sklearn.linear_model import LogisticRegressionCV
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import RadiusNeighborsClassifier
from sklearn import tree
from sklearn.externals.six import StringIO
import pydot 

def get_dif(result, cuisine):
    d = [] 
    for i in range(len(result)):
        d.append(result[i] == cuisine[i])
    
    s = float(sum(d))
    dif = float(s/len(d))*100.
    return dif
    
def check_accuracy(result1, result2, result3, c_tr, c_cv, c_t):
    print "Training accuracy:"
    print get_dif(result1, c_tr)
    print "Cross-validation accuracy:"
    print get_dif(result2, c_cv)
    print "Test accuracy:"
    print get_dif(result3, c_t)


#read data
data = pd.read_json('train.json')
data.head() #show how data looks

recipe_num,y = data.shape
training_size = round(recipe_num*80./100.)
cv_size = round((recipe_num - training_size)/2)
testing_size = recipe_num - training_size - cv_size

print training_size, cv_size, testing_size #size of datasets

ingredients = data['ingredients'] #ripping out only the ingredients

#add some preprocessing: stemming words
ps = PorterStemmer()
for line in ingredients:
    for word in line:
        ps.stem(word)
        
words_list = [' '.join(x) for x in ingredients]

#split data set
train = words_list[:int(training_size)]
cv = words_list[int(training_size)+1:int(training_size)+int(cv_size)+1]
test = words_list[-1*int(testing_size):]
 
#create a bag of words for all sets       
vectorizer = CountVectorizer() #not restricting number of features

bag_of_training_words = vectorizer.fit(train)
bag_of_training_words = vectorizer.transform(train).toarray()
print(bag_of_training_words.shape)
print type(bag_of_training_words)
#bag_of_training_words = np.array(bag_of_training_words).astype(np.float)
bag_of_training_words = np.array(bag_of_training_words, dtype = 'float_')
print '!'
print(bag_of_training_words.shape)
print type(bag_of_training_words)

bag_of_cv_words = vectorizer.fit(train)
bag_of_cv_words = vectorizer.transform(cv).toarray()
print(bag_of_cv_words.shape)

bag_of_testing_words = vectorizer.fit(train)
bag_of_testing_words = vectorizer.transform(test).toarray()
print(bag_of_testing_words.shape)

#separate cuisine data
cuisine = data["cuisine"].values.tolist()
cuisine_for_training = cuisine[:int(training_size)]
cuisine_for_cv = cuisine[int(training_size)+1:int(training_size)+int(cv_size)+1]
cuisine_for_testing = cuisine[-1*int(testing_size):]
#print cuisine_for_testing

print len(cuisine_for_training), len(cuisine_for_cv), len(cuisine_for_testing)
'''
### 1. Random Forest Classifier

print "Random Forest"

# train model
forest = RandomForestClassifier(n_estimators = 100)
forest = forest.fit( bag_of_training_words, cuisine_for_training )

# predict results
result1 = forest.predict(bag_of_training_words)
result2 = forest.predict(bag_of_cv_words)
result3 = forest.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)

# picture

dot_data = StringIO() 
tree.export_graphviz(forest.estimators_[0], out_file=dot_data) 
graph = pydot.graph_from_dot_data(dot_data.getvalue()) 
graph.write_pdf("rf.pdf") 


# results
# (31819, 2890)
# (7955, 2890)
# 99.9685722367
# 76.4676304211

# 1000 trees
# (31819, 2890)
# (7955, 2890)
# 99.9685722367
# 76.5933375236


### 2. SVM- LinearSVC
print "Support-vector machine: LinearSVC"

# train model
svm = svm.LinearSVC(dual=False, max_iter = 1000)
svm.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = svm.predict(bag_of_training_words)
result2 = svm.predict(bag_of_cv_words)
result3 = svm.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)


### 3. SVM
print "Support-vector machine"

# train model
svm = svm.SVC(decision_function_shape = 'ovo', max_iter = 1000)
svm.fit(bag_of_training_words, cuisine_for_training)

print 'done training'

# predict results
result1 = svm.predict(bag_of_training_words)
result2 = svm.predict(bag_of_cv_words)
result3 = svm.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing) 


### 4. Logistic regression
print 'Logistic regression'

model_regression = LogisticRegressionCV(n_jobs=-1, max_iter=10) #scoring=log_loss
model_regression.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = model_regression.predict(bag_of_training_words)
result2 = model_regression.predict(bag_of_cv_words)
result3 = model_regression.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)


### 5. SGDClassifier
print 'SGDClassifier'

clf = SGDClassifier(loss="hinge", penalty="l2")
clf.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = clf.predict(bag_of_training_words)
result2 = clf.predict(bag_of_cv_words)
result3 = clf.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)


### 6. Perceptron

print 'Perceptron'

clf = Perceptron()
clf.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = clf.predict(bag_of_training_words)
result2 = clf.predict(bag_of_cv_words)
result3 = clf.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)


### 7. CART
print 'CART'

clf = tree.DecisionTreeClassifier()
clf = clf.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = clf.predict(bag_of_training_words)
result2 = clf.predict(bag_of_cv_words)
result3 = clf.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)

# picture

#tree.export_graphviz(clf, out_file='tree.dot')

dot_data = StringIO() 
tree.export_graphviz(clf, out_file=dot_data) 
graph = pydot.graph_from_dot_data(dot_data.getvalue()) 
graph.write_pdf("wc.pdf") 

### 8. NuSVM
print "NuSVM"

# train model
svm = svm.NuSVC(nu=0.9, decision_function_shape = 'ovr')
svm.fit(bag_of_training_words, cuisine_for_training)

print 'done training'

# predict results
result1 = svm.predict(bag_of_training_words)
result2 = svm.predict(bag_of_cv_words)
result3 = svm.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)

'''
### 9. KNeighborsClassifier
print "KNeighborsClassifier"

# train model
neigh = KNeighborsClassifier(n_neighbors=15)
neigh.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = neigh.predict(bag_of_training_words)
result2 = neigh.predict(bag_of_cv_words)
result3 = neigh.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)
'''
### 10. RadiusNeighborsClassifier
print "RadiusNeighborsClassifier"

# train model
neigh = RadiusNeighborsClassifier(radius=5.0, outlier_label='italian')
neigh.fit(bag_of_training_words, cuisine_for_training)

# predict results
result1 = neigh.predict(bag_of_training_words)
result2 = neigh.predict(bag_of_cv_words)
result3 = neigh.predict(bag_of_testing_words)

# count accuracy
check_accuracy(result1, result2, result3, cuisine_for_training, cuisine_for_cv, cuisine_for_testing)

'''
